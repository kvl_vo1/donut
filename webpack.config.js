const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

const SourceDir = path.join(__dirname, 'src');
const DistDir = path.join(__dirname, 'dist');

const isDev = process.env.NODE_ENV === 'development';

module.exports = {
  entry: [
    'babel-polyfill',
    path.join(SourceDir, '/app.js')
  ],

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.js'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve(__dirname, 'src'),
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            presets: ['@babel/react', '@babel/env'],
            plugins: [/*'react-hot-loader/babel',*/ 'transform-class-properties', '@babel/proposal-class-properties', 'transform-regenerator']
          }
        }
      },
      {
        test: /\.css$/,
        use: [
          'css-hot-loader',
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // only enable hot in development
              hmr: isDev,
              // if hmr does not work, this is a forceful method.
              // reloadAll: true,
            },
          },
          'css-loader'
        ],
      },
      {
        test: /\.scss$/,
        use: [
          'css-hot-loader',
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // only enable hot in development
              hmr: isDev,
              // if hmr does not work, this is a forceful method.
              //reloadAll: true,
            },
          },
          'css-loader',
          'sass-loader?outputStyle=expanded' //uncompressed compressed expanded
        ],
      },
      {
        test: /\.(png|jp(e*)g)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              context: 'src/images/',
              outputPath: 'images/',
              //useRelativePath: true
            }
          },
        ]
      },
    ]
  },

  devServer: {
    contentBase: SourceDir,
    watchContentBase: true,
    historyApiFallback: true,
    compress: true,
    stats: 'errors-only', //"normal",
    open: true,
    inline: true,
    hot: true
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: 'Donut',
      rs: 'style/reset.css',
      jq: 'js/jquery/jquery.min.js',
      bs: ['js/bootstrap/css/bootstrap.min.css', 'js/bootstrap/bootstrap.min.js'],
      sl: ['js/owl/assets/owl.carousel.min.css', 'js/owl/assets/owl.theme.default.min.css', 'js/owl/owl.carousel.min.js'],
      // minify: {
      //     collapseWhitespace: true
      // },
      // hash: true,
      template: path.join(SourceDir, 'index.html')
    }),

    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // all options are optional
      filename: './style/style.css',
      chunkFilename: '[id].css',
      // Enable to remove warnings about conflicting order
      ignoreOrder: false,
    }),

    new CopyWebpackPlugin([
      {
        context: 'src/images',
        from: '**/*.png',
        to: path.join(DistDir, 'images')
      },
      {
        context: 'src/images',
        from: '**/*.jp*g',
        to: path.join(DistDir, 'images')
      },
      {
        context: 'src/style/fonts',
        from: '**/*.*',
        to: path.join(DistDir, 'style/fonts')
      },
      {
        context: 'node_modules/reset-css/',
        from: 'reset.css',
        to: path.join(DistDir, 'style')
      }
    ]),

    new webpack.HotModuleReplacementPlugin()
  ]
};